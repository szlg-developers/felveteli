<?php

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD);

echo 'Adatbázis létrehozása...'.PHP_EOL;

mm_multi_query($conn, "
	CREATE DATABASE `felveteli`;
	USE `felveteli`;

	CREATE TABLE `diak` 
	(
		`om_azonosito`varchar(11) NOT NULL PRIMARY KEY,
		`nev` varchar(255) NOT NULL,
		`osztaly` varchar(255)
	);
");

while(!($password = mm_read_password('Admin jelszó (biztonságos jelszót adjon meg!): ')))
	echo 'Kérem adjon meg egy jelszót!'.PHP_EOL;
mm_add_config('ADMIN_PSW', $password);

mm_add_config('FELIRAT_ISKOLA', '<iskola>', 'Iskola neve');
mm_add_config('FELIRAT_TANEV', '<tanév>', 'Tanév (pl. 2020/2021.)');

$conn->close();

echo PHP_EOL;
