<?php 
	session_start();
	include '../../config.php';
	include 'includes/csrf_check.php';

	if(!isset($_POST['om_az']) || !isset($_POST['csrf_hash']))
	{
		exit('Error: Unset javascript variable!');
	}

	csrf_check('om',$_POST['csrf_hash']);

	if(strlen($_POST['om_az']) > 11 || strlen($_POST['om_az']) < 11)
	{
		exit(json_encode(array('Error' => 'length')));
	}
	if(substr($_POST['om_az'], 0, 1) != '7')
	{
		exit(json_encode(array('Error' => 'not seven')));
	}
	if(!is_numeric($_POST['om_az']))
	{
		exit(json_encode(array('Error' => 'not num')));
	}

	$conn = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASSWORD,'felveteli');
	$query = $conn->prepare('SELECT `nev`,`osztaly` FROM `diak` WHERE `om_azonosito` = ?');
	$query->bind_param('s',$_POST['om_az']);
	$query->execute();
	$query->bind_result($d_name,$d_class);
	$query->fetch();
	if(is_null($d_name))
	{
		$query->close();
		exit(json_encode(array('Error' => 'variable null')));
	}
	$diak_array = array('Error'=> 'none', 'name' => $d_name, 'class' => $d_class);
	$query->close();
	echo json_encode($diak_array);
	$conn->close();
?>