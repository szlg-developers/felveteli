<?php 
	function csrf_check($key, $hash)
	{
		if($hash != $_SESSION['csrf'][$key] || $_SESSION['csrf'][$key] == '')
		{
			die(json_encode(array
			(
				'success' => false,
				'message' => 'CSRF failure',
			)));
		}
	}
 ?>