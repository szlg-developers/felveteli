<?php 
	function generate_hash($length)
	{
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$hash = '';
		$max = strlen($chars) - 1;
		for ($i = 0; $i < $length; ++$i)
		{
			$hash .= $chars[random_int(0, $max)];
		}
		return $hash;
	}
 ?>