<?php 
	function csrf_generate($key)
	{
		include_once __DIR__.'/generate_hash.php';

		$hash = generate_hash(64);

		$_SESSION['csrf'][$key] = $hash;

		return $hash;
	}
 ?>