<?php 
	session_start();
	if(!isset($_SESSION['admin']))
	{
		exit();
	}

	include '../../config.php';
	include 'includes/csrf_check.php';

	if(!isset($_POST['csrf_key']) || !isset($_POST['csrf_hash']))
	{
		exit(json_encode(array('Error' => 'Undefined javascript variable')));
	}

	if(!isset($_FILES['file_to_up']))
	{
		exit(json_encode(array('Error' => 'File not uploaded')));
	}

	if(pathinfo($_FILES['file_to_up']['name'], PATHINFO_EXTENSION) == 'txt')
	{

		$conn = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASSWORD,'felveteli');
		$file = fopen($_FILES['file_to_up']['tmp_name'],'r');
		$line_num = 0;
		$array = [];
		while (!feof($file)) 
		{
			$line = fgets($file);
			++$line_num;
			$line2 = preg_split("/[\t]/", $line);
			if(substr($line2[0], 0,1) != '7')
			{
				fclose($file);
				unlink($_FILES['file_to_up']['tmp_name']);
				exit(json_encode(array('Error' => 'not seven', 'line'=>$line_num)));
			}
			if(strlen($line2[0]) != 11)
			{
				fclose($file);
				unlink($_FILES['file_to_up']['tmp_name']);
				exit(json_encode(array('Error' => 'length', 'line'=>$line_num)));
			}
			if (!isset($line2[2])) 
			{
				$line2[2] = NULL;
			}
			$query2 = $conn->prepare('SELECT `om_azonosito` FROM `diak` WHERE `om_azonosito` = ?');
			$query2->bind_param('s',$line2[0]);
			$query2->execute();
			$res = $query2->num_rows;
			if($res > 0)
			{
				fclose($file);
				unlink($_FILES['file_to_up']['tmp_name']);
				exit(json_encode(array('Error' => 'same_id', 'line'=>$line_num)));
			}
			$query2->close();
			$count_array2 = count($array);
			for($o = 0; $o < $count_array2; ++$o)
			{
				if($array[$o]['om_az'] == $line2[0])
				{
					fclose($file);
					unlink($_FILES['file_to_up']['tmp_name']);
					exit(json_encode(array('Error' => 'same_id', 'line'=>$line_num)));
				}
			}

			$array_struct = array('om_az' => trim($line2[0]), 'name' => trim($line2[1]),'osztaly'=> trim($line2[2]));
			array_push($array,$array_struct);
			
		}
		fclose($file);

		$conn->query('TRUNCATE TABLE `diak`');

		$count_array = count($array);
		for($i = 0; $i < $count_array; ++$i)
		{
			$query = $conn->prepare('INSERT INTO `diak` (`om_azonosito`,`nev`,`osztaly`) VALUES (?,?,?)');
			$query->bind_param('sss',$array[$i]['om_az'],$array[$i]['name'],$array[$i]['osztaly']);
			$query->execute();
			$query->close();
		}
		unlink($_FILES['file_to_up']['tmp_name']);
		echo json_encode(array('Error' => 'none'));
		$conn->close();
	}	
	else
	{
		exit('File not txt.');
	}
?>