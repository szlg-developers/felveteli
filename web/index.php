<?php 
	include '../config.php';
	session_start();
	include 'backend/includes/csrf_generate.php';
	$hash = csrf_generate('om');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Felvételi eredmények</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="assets/style/design.css">
	<link rel="stylesheet" type="text/css" href="assets/style/pop_up.css">
	<script type="text/javascript" src="assets/script/javascript.js"></script>
	<script type="text/javascript" src="assets/script/sheet_functions.js"></script>
	<script type="text/javascript"> const csrf_hash = '<?php echo $hash; ?>';</script>
</head>
<body>
	<div id="om_div">
		<h2 id="om_h">A <?php echo FELIRAT_ISKOLA ?> <?php echo FELIRAT_TANEV ?> tanévére felvételt nyert tanulók</h2>
		<h2 id="om_h">OM azonosító</h2>
		<input id="om_in" type="text" name="om_in" maxlength="11" placeholder="OM azonosító" autofocus>
		<button id="om_btn" onclick="if(!act_popup) send_data();">Eredmény</button>
	</div>
	<div id="inform_div" style="display:none;">
		<p id="m_p"></p>
		<p id="d_p"></p>
	</div>
</body>
</html>