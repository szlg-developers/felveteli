let act_popup = null;

window.addEventListener('load',function()
{
 	let om_field = document.getElementById('om_in');
	window.location.hash = '';
	document.body.children[0].children[1].focus();
	om_field.addEventListener('keyup',(event)=>
	{
		if(event.keyCode === 13)
		{
			if(!act_popup)
			{
				event.preventDefault();
				send_data();
			}
			else if(act_popup != 1)
			{
				act_popup.close();
			}
		}

	});
});

window.addEventListener('hashchange',function()
{
	if(window.location.hash != '#tajekoztat')
	{
		act_popup = null;
		document.body.children[0]['style']['display'] = 'flex';
		document.body.children[1]['style']['display'] = 'none';
		document.body.children[0].children[2].select();
		document.body.children[0].children[2].focus();
		document.body.children[1].children[0].innerHTML = '';
		document.body.children[1].children[1].innerHTML = '';
	}

});
function send_data()
{
	act_popup = 1;
	window.location.hash = 'tajekoztat';
	let pop_up_long = new pop_up_message_box('Figyelmeztetés','A karakterek számának 11-nek kell lennie!');
	pop_up_long.close = function() { this.base['style']['display'] = 'none'; act_popup = null; document.body.children[0].children[1].focus(); };
	let pop_up_seven = new pop_up_message_box('Figyelmeztetés','Az okmányazonosító csak 7-essel kezdődhet!');
	pop_up_seven.close = function() { this.base['style']['display'] = 'none'; act_popup = null; document.body.children[0].children[1].focus(); };
	let pop_up_nan = new pop_up_message_box('Figyelmeztetés','A beírt azonosító nem szám!');
	pop_up_nan.close = function() { this.base['style']['display'] = 'none'; act_popup = null; document.body.children[0].children[1].focus(); };
	let om_in = document.body.children[0].children[2]['value'];
	if(!isNaN(om_in))
	{
		if(om_in.length == 11)
		{
			let first_char = om_in.substr(0,1);
			if(first_char == 7)
			{
				let form_data = new FormData();
				form_data.append('csrf_hash',csrf_hash);
				form_data.append('csrf_key','om');
				form_data.append('om_az',om_in);
				fetch('backend/get_student.php',{'method':'POST','body':form_data,'credentials':'same-origin'}).then((response)=>
				{
					return response.json().then((res_txt)=>
					{;
						if(res_txt['Error'] == 'none')
						{
							
							document.body.children[0]['style']['display'] = 'none';	
							document.body.children[1].children[0].innerHTML = 'Tisztelt '+ res_txt['name'] + '!' ;
							document.body.children[1].children[1].innerHTML = '';
							document.body.children[1].children[1].appendChild(document.createTextNode('Örömmel értesítjük, hogy felvételt nyert a Kőbányai Szent László Gimánzium \n ' + res_txt['class'] + ' szakára! \n A felvételi eljárás hivatalos értesítését postai úton jutattjuk el minden jelentkezőnek 2020. április 30-ig.'));
							document.body.children[1]['style']['display'] = 'flex';
							
						}
						else if(res_txt['Error'] == 'variable null')
						{
							document.body.children[0]['style']['display'] = 'none';
							document.body.children[1].children[1].innerHTML = '';
							document.body.children[1]['style']['display'] = 'flex';
							document.body.children[1].children[1].innerHTML = om_in + ' OM azonosítójú diák nem nyert felvételt a Kőbányai Szent László Gimnáziumba.'

						}
					});		
				});
			}
			else
			{
				act_popup = pop_up_seven;
				pop_up_seven.create();
				pop_up_seven.show();
			}
		}
		else
		{
			act_popup = pop_up_long;
			pop_up_long.create();
			pop_up_long.show();
		}
	}
	else
	{
		let form_data = new FormData();
		form_data.append('csrf_hash',csrf_hash);
		form_data.append('csrf_key','om');
		form_data.append('om_az',om_in);
		fetch('backend/admin_in.php',{'method':'POST','body':form_data,'credentials':'same-origin'}).then((response)=>
		{
			return response.json().then((res_txt)=>
			{
				if(res_txt['Error'] == 'none')
				{
					window.location.href = 'admin.php';
				}
				else
				{
					act_popup = pop_up_nan;
					pop_up_nan.create();
					pop_up_nan.show();
				}
			});		
		});
	}	
}