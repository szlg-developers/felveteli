// generate_html([
// 	{
// 		'name':'div',
// 		'parent':{'elem':obj,'insert_after':false, 'insert_before':false,'num':null},
// 		'properties':{'class':['name','name'],'style':{'display':'none'},'inner_html':'html'},
// 		'events':{},
// 		'children':[{'name':'img','properties':{}},{},{},{}]
// 	},{},{},{},{}]);
// tags: [{'name': 'name','parent':{'elem':the parent element, 'insert_after:true/false, 'insert_before':true/false, 'num':where to insert element} 
//'properties':[{'class':[],'style':{}}],'events':{'event_name':function(){}} , 'children':[{'name':'name',children:[{}]}]]
//you have to add the class names in an array
//you have to add styles in an array
//you can add styles in it
async function generate_for_children(tags = {},parent_obj)
{
	let elem = document.createElement(tags['name']);
	let obj_keys = Object.keys(tags['properties']);
	for(let o = 0; o < obj_keys.length; ++o)
	{
		if(obj_keys[o] == 'class')
		{
			for(let i = 0; i < tags['properties'][obj_keys[o]].length;++i)
			{
				elem.classList.add(tags['properties'][obj_keys[o]][i]);
			}
		}
		else if(obj_keys[o] == 'inner_html')
		{
			elem.appendChild(document.createTextNode(tags['properties'][obj_keys[o]]));
		}
		else if(obj_keys[o] == 'style')
		{
			let obj_keys_style = Object.keys(tags['properties'][obj_keys[o]]);
			for(let i = 0; i < obj_keys_style.length; ++i)
			{
				elem['style'][obj_keys_style[i]] = tags['properties'][obj_keys[o]][obj_keys_style[i]];
			}
		}
		else
		{			
			elem[obj_keys[o]] = tags['properties'][obj_keys[o]];
		}
	}
	let event_obj_keys = Object.keys(tags['events']);

	for(let i = 0; i < event_obj_keys.length; ++i)
	{
		elem.addEventListener(event_obj_keys[i],tags['events'][event_obj_keys]);
	}
	parent_obj.appendChild(elem);
	if(tags['children'].length > 0)
	{
		for(let i = 0; i < tags['children'].length; ++i)
		{
			generate_for_children(tags['children'][i],elem);
		}
	}
}
async function generate_html(tags = [{}])
{
	for(let tagnum = 0; tagnum < tags.length; ++tagnum)
	{
		let elem = document.createElement(tags[tagnum]['name']);
		let obj_keys = Object.keys(tags[tagnum]['properties']);
		//tulajdonságok hozzáadása
		for(let o = 0; o < obj_keys.length; ++o)
		{
			if(obj_keys[o] == 'class')
			{
				for(let i = 0; i < tags[tagnum]['properties'][obj_keys[o]].length;++i)
				{
					elem.classList.add(tags[tagnum]['properties'][obj_keys[o]][i]);
				}
			}
			else if(obj_keys[o] == 'inner_html')
			{
				elem.appendChild(document.createTextNode(tags[tagnum]['properties'][obj_keys[o]]));
			}
			else if(obj_keys[o] == 'style')
			{
				let obj_keys_style = Object.keys(tags[tagnum]['properties'][obj_keys[o]]);
				for(let i = 0; i < obj_keys_style.length; ++i)
				{
					elem['style'][obj_keys_style[i]] = tags[tagnum]['properties'][obj_keys[o]][obj_keys_style[i]];
				}
			}
			else
			{			
				elem[obj_keys[o]] = tags[tagnum]['properties'][obj_keys[o]];
			}
		}
		let event_obj_keys = Object.keys(tags[tagnum]['events']);
		for(let i = 0; i < event_obj_keys.length; ++i)
		{
			elem.addEventListener(event_obj_keys[i],tags[tagnum]['events'][event_obj_keys]);
		}
		if(tags[tagnum]['children'].length > 0)
		{
			for(let i = 0; i < tags[tagnum]['children'].length; ++i)
			{
				generate_for_children(tags[tagnum]['children'][i],elem);
			}
		}
		if(tags[tagnum]['parent'] == null)
		{
			let bdy = document.getElementsByTagName('body')[0];
			bdy.appendChild(elem);
		}
		else if(tags[tagnum]['parent']['insert_before'] == true)
		{
			//1. meghatározzuk az elemet ahova be szeretnénk illeszteni, majd meghatározzuk hogy melyik elem elé illesszük be
			tags[tagnum]['parent']['elem'].insertBefore(elem,tags[tagnum]['parent']['elem'].children[tags[tagnum]['parent']['num']]);
		}
		else if(tags[tagnum]['parent']['insert_after'] == true)
		{
			tags[tagnum]['parent']['elem'].insertAfter(elem,tags[tagnum]['parent']['elem'].children[tags[tagnum]['parent']['num']]);
		}
		else
		{
			tags[tagnum]['parent']['elem'].appendChild(elem); 
		}
	}
}

function get_values(root,tags,input_type_excepiton = null)
{
	let list = new Array();
	list = construct(root,list,tags,input_type_excepiton);
	return list;
}

function construct_recursive_array(root,children_array)
{
	if(root.children.length > 0 && root.tagName.toLowerCase() != 'select')
	{
		for(let i = 0; i < root.children.length; ++i)
		{
			construct_recursive_array(root.children[i],children_array)
		}
		return children_array;
	}
	else
	{
		children_array.push(root);
		return children_array;
	}
}

function insert_values_to(root,output_list,list_tags = null)
{
	let children_array = new Array();
	construct_recursive_array(root,children_array);
	let i = 0;
	let o = 0;
	while(i < children_array.length && o < output_list.length)
	{
		if(list_tags == null)
		{
			children_array[i] == output_list[o];
			++i;
			++o;
		}
		else
		{
			for(let p = 0; p < list_tags.length; ++p)
			{

				if(children_array[i].tagName.toLowerCase() == 'input' && children_array[i]['type'] == list_tags[p])
				{
					if( list_tags[p] == 'radiobutton' ||  list_tags[p] == 'checkbox')
					{
						if(children_array[i].value == output_list[o])
						{
							children_array[i].checked = true;
							++o;
						}
					}
					else if( list_tags[p] == 'select')
					{
						for(let q = 0; q < children_array[i].children.length; ++q)
						{
							if(children_array[i].children[q].value == output_list[o])
							{
								children_array[i].selectedIndex = q;
								++o;
								break;
							}
						}
					}
					else
					{
						children_array[i].value = output_list[o];
						++o;
					}
				}
				else if(children_array[i].tagName.toLowerCase() == list_tags[p])
				{
					children_array[i].value = output_list[o];
					++o;
				}
			}
			++i;
		}
	}
}

//tetszőleges elementekből képes beolvasni adatokat vagy éppen kiüríteni azokat
//listát akkor ürít ki ha nem adunk meg neki input listát
//a root változó a kiinduló elem
//a tags a beolvasandó elemeket tartalmazza
//az input_type_exceptionben meg lehet adni azon input típusokat amelyeket kihagyjon a beolvasásból vagy éppen kiürítésből
function construct(root,input_list,tags,input_type_excepiton = null)
{
	if(root.children.length > 0 && root.tagName.toLowerCase() != 'select')
	{
		for(let i = 0; i < root.children.length;++i)
		{
			construct(root.children[i],input_list,tags,input_type_excepiton);
		}
		return input_list;
	}
	else
	{
		for(let i = 0; i < tags.length; ++i)
		{

			if(root.tagName.toLowerCase() == tags[i] || (root.tagName.toLowerCase() == 'select' && tags[i] == 'select_all'))
			{
				if(input_type_excepiton != null)
				{
						let contains = false;
						for(let o = 0; o < input_type_excepiton.length;++o)
						{
							if(input_type_excepiton[o] == root['type'] || input_type_excepiton[o] == root.tagName.toLowerCase())
							{
								contains = true;
								break;
							}
						}
						if(!contains)
						{
							if(root['type'] == 'radiobutton' || root['type'] == 'checkbox')
							{
								if(root.checked)
								{
									if(input_list == null)
									{
										root.checked = false;
									}
									else
									{
										input_list.push(root.value);
									}
								}
							}
							else if(root.tagName.toLowerCase() == 'select' || (root.tagName.toLowerCase() == 'select' && tags[i] == 'select_all'))
							{
								if(tags[i] == 'select')
								{
									if(input_list == null)
									{
										root.selectedIndex = 0;
									}
									else
									{

										input_list.push(root.options[root.selectedIndex].value);
									}
								}
								else
								{
									if(input_list == null)
									{
										del_elements([],root,['all']);									
									}
									else
									{
										let select_array = new Array();
										for(let i = 0; i < root.children.length; ++i)
										{
											select_array.push(root.options[i].value);
										}
										input_list.push(select_array);
									}
								}
							}
							else if(root['type'] == 'file')
							{
								if(input_list == null)
								{
									root.value = "";
								}
								else
								{
									let file_array = new Array();
									for(let i = 0; i < root.files.length; ++i)
									{
										file_array.push(root.files[i]);
									}
									input_list.push(file_array);
								}
							}
							else
							{	
								if(input_list == null)
								{
									root.value = '';
								}
								else
								{
									input_list.push(root.value);
								}
							}
					}
					break;
				}
				else
				{
					if(root['type'] == 'radiobutton' || root['type'] == 'checkbox')
					{
						if(root.checked)
						{
							if(input_list == null)
							{
								root.checked = false;
							}
							else
							{
								input_list.push(root.value);
							}
						}
					}
					else if(root.tagName.toLowerCase() == 'select')
					{
						if(input_list == null)
						{
							root.selectedIndex = 0;
						}
						else
						{
							input_list.push(root.options[root.selectedIndex].value);
						}
					}
					else if(root['type'] == 'file')
					{
						input_list.push(root.files[0]);
					}
					else
					{	
						if(input_list == null)
						{
							root.value = '';
						}
						else
						{
							input_list.push(root.value);
						}
					}
					break;
				}
			}
		}
		return input_list;
	}
}
//ha az elementsnek adunk meg elemet akkor az elementeket fogja törölni ha a parentsbe adunk meg számokat azokat az indexű elemeket fogja törölni
//ha a parentbe 'all' paramétert írunk be az összes elementet törölni fogja
//ha az elem_num változót true-ra állítjuk akkor egy adott számú elementet fog törölni, csak egy szám szerepelhet a parentban
//ha a from backet állítjuk és az elem_num true akkor a tömb hátuljából fogja törölni az elementeket
//ha a from_back és a from_front is true akkor elölről illetve hátulról is fog elemeket törölni
//ha az interval true akkor két intervallumon belüli elementeket fogja törölni, csak két szám szerepelhet a parent tömbben
function del_elements(elements = [],parent_elem = null,parent = [],interval = false,elem_num = false,from_back = false,from_front=true)
{
	if(elements.length > 0)
	{
		for(let i = 0; i < elements.length; ++i)
		{
			elements[i].parentElement.removeChild(elements[i]);
		}
	}
	else
	{
		if(parent_elem != null)
		{
			
			if(parent.length == 1 && parent[0] == 'all')
			{
				let o = 0;
				while(o<parent_elem.children.length)
				{				
					parent_elem.removeChild(parent_elem.children[o]);
				}
				
			}
			else if(elem_num == true && from_back == false && from_front == true)
			{
				let o = 0;
				for(let i = 0; i < parent[0]; ++i)
				{
					if(o < parent_elem.children.length)
					{
						parent_elem.removeChild(parent_elem.children[o]);
					}
					else
					{
						break;
					}
				}
			}
			else if(elem_num == true && from_back == true)
			{
				let o = parent_elem.children.length-1;
				for(let i = 0; i < parent[0]; ++i)
				{
					if(o > 0)
					{
						parent_elem.removeChild(parent_elem.children[o]);
					}
					else
					{
						break;
					}
				}
			}
			else if(elem_num == true && from_back == true && from_front == true)
			{
				
				let p = 0;
				for(let i = 0; i < parent_elem[0]; ++i)
				{

					if(p < parent_elem.children.length)
					{
						parent_elem.removeChild(parent_elem.children[p]);
					}
					else
					{
						break;
					}
				}
				let o = parent_elem.children.length-1;
				if(o > 0)
				{
					for(let i = 0; i < parent[0]; ++i)
					{
						if(o > 0)
						{
							parent_elem.removeChild(parent_elem.children[o]);
						}
						else
						{
							break;
						}
					}
				}
			}
			else if(interval == true && elem_num == false)
			{
				let o = parent[0];
				let elem = parent_elem.children[parent[0]];
				for(let i = parent[0]; i <= parent[1]; ++i)
				{
					if(o < parent_elem.children.length && o > -1 && parent[1] >= o && parent[1] < parent_elem.children.length && parent[1]> -1)
					{
						parent_elem.removeChild(parent_elem.children[o]);
					}
					else
					{
						break;
					}
				}
			}
			else
			{
				let o = 0;
				let copy = parent_elem.children;
				for(let i = 0; i < parent.length; ++i)
				{
					o = parent[i];
					for(let p = 0; p < copy.length; ++p)
					{
						for(let q = 0; q < parent_elem.children.length; ++q)
						{
							if(copy[p] == parent_elem.children[q] && p == o)
							{
								parent_elem.removeChild(parent_elem.children[q]);
							}
						}
					}
				}
			}
		}
	}
}
function generate_random(length,content = 'all')
{
	let generated_string = '';
	let generate_from;
	if(content == 'all')
	{
		generate_from = '_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZöüóőúéáűíÖÜÓŐÚÉŰÁÍłŁß$¤äđĐÄ€';
	}
	else if(content == 'abc_num')
	{
		generate_from = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	}
	else if(content = 'abc_cap')
	{
		generate_from = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	}
	else if(content = 'abc')
	{
		generate_from = 'abcdefghijkjlmnopqrstuvwxyz';
	}
	let from = generate_from.split('');
	for(let i = 0; i < length; ++i)
	{
		let random_num = Math.floor(Math.random()*from.length);
		generated_string += from[random_num];
	}
	return generated_string;
}

//felugró ablak generálása
//tulajdonságok title: a box címe
//leírás: a box közlő címe
//be kell hozzá importálni az általános felugró ablak modellt
//az id_names mezőben meg kell adni az általános felugró ablak modellben található idket és classokat
//a buttonsban a gombokat lehet hozzzáadni pl: [{'btn_value':'Mégse','btn_id':'megse_','btn_color':'#e2584d','btn_event':()=>{pop_up.close();pop_up.remove_all_events('oke_alert')}}
//az x_btn-el lehet állítani hogy legyen e benne x-gomb
class pop_up_message_box
{
	constructor(title='', description='',class_name='alert')
	{
		this.base = '';
		this.buttons = [];
		this.class_name=class_name;
		this.title=title;
		this.description=description;
		
	}
	create(buttons=[],x_btn=true)
	{
		generate_html(
		[
		{
			'name':'div',
			'parent':null,
			'properties':{'class':['alap_'+this.class_name],'style':{'display':'none'}},
			'events':{},
			'children':
			[
			{
				'name':'div',
				'properties':{'class':['tartalom_'+this.class_name]},
				'events':{},
				'children':[
				{
					'name':'div',
					'properties':{'class':['x_'+this.class_name]},
					'events':{},
					'children':[]
				},
				{
					'name':'p',
					'properties':{'class':['szoveg_'+this.class_name,'cim_'+this.class_name],'inner_html':this.title},
					'events':{},
					'children':[]
				},
				{
					'name':'p',
					'properties':{'class':['szoveg_'+this.class_name,'leiras_'+this.class_name],'inner_html':this.description},
					'events':{},
					'children':[]
				},
				{
					'name':'div',
					'properties':{'class':['gombok_'+this.class_name]},
					'events':{},
					'children':[] 
				}]
			}
			]	
		}]);
		this.base = document.body.lastElementChild;
		let button_holder = this.base.children[0].children[3];
		if(x_btn)
		{
			generate_html([
				{
				'name':'p',
				'parent':{'elem':this.base.children[0].children[0],'insert_after':false,'insert_before':false,'num':null},
				'properties':{'inner_html':'×','id':'x_gomb'},
				'events':{'click':()=>{this.close();}},
				'children':[]
				}
			]);
		}
		for(let i = 0;i < buttons.length; ++i)
		{
			generate_html([{
				'name':'input',
				'parent':{'elem':button_holder,'insert_after':false,'insert_before':false,'num':null},
				'properties':{
					'type':'button',
					'value':buttons[i]['btn_value'],
					'id':buttons[i]['btn_id']+this.id_name,
					'class':['gomb_'+this.id_name['class']],
					'style':{
					'text-align':'right',
					'margin-top': '10px',
					'background-color':buttons[i]['btn_color'],
					'color':'white',
					'border':'2px solid '+buttons[i]['btn_color']
				}
				},
				'events':{'click':buttons[i]['btn_event']},
				'children':[]
				}]);
			this.buttons.push(button_holder.children[i]);
		}
	}
	close()
	{
		this.base['style']['display'] = 'none';
	}
	show()
	{
		this.base['style']['display'] = 'inline-block';
	}
	btn_add_event(btn_index,event_name='click',event_to_add)
	{
		this.buttons[btn_index].addEventListener(event_name,event_to_add);
	}
	btn_remove_all_events(btn_index)
	{
		let old_element = this.buttons[btn_index];
		let new_element = old_element.cloneNode(true);
		old_element.parentNode.replaceChild(new_element, old_element);
	}
	add_description(text)
	{
		this.description += ';'+text;
		this.base.children[0].children[2].appendChild(document.createTextNode(text));
	}
}