function upload_txt(elem)
{
	let pop_up_ext = new pop_up_message_box('Figyelmeztetés','A fájl kiterjesztése csak zip lehet!');
	let pop_up_not_chosen = new pop_up_message_box('Figyelmeztetés','Nincs fájl kiválasztva!');
	let pop_up_err = new pop_up_message_box('Figyelmeztetés');
	let pop_up_notbool = new pop_up_message_box('Figyelmeztetés');
	let pop_up_same_id = new pop_up_message_box('Figyelmeztetés');
	let pop_up_seven = new pop_up_message_box('Figyelmeztetés');
	let pop_up_done = new pop_up_message_box('Információ','Sikeres feltöltés!');
	let pop_up_uploading = new pop_up_message_box('Információ','Feltöltés folyamatban...');
	let file_elem = elem.parentElement.children[0].files[0];
	if(typeof file_elem !== 'undefined')
	{
		let ext = file_elem.name.split('.').pop();
		if(ext == 'txt')
		{
			pop_up_uploading.create();
			pop_up_uploading.show();
	 		let form_data = new FormData();
	 		form_data.append('csrf_hash',csrf);
	 		form_data.append('csrf_key','admin_upload');
	 		form_data.append('file_to_up',file_elem);
	 		fetch('backend/upload_students.php',{'method':'POST','body':form_data,'credentials':'same-origin'}).then((response)=>
	 		{
	 			return response.json().then((res_txt)=>
	 			{
	 				pop_up_uploading.close();
	 				if(res_txt['Error'] == 'length')
	 				{
	 					pop_up_err.create();
	 					pop_up_err.add_description('Nem elég hosszú/Túl hosszú az om azonosító Sor:'+res_txt['line']);
	 					pop_up_err.show();
	 				}
	 				else if(res_txt['Error'] == 'same_id')
	 				{
						pop_up_same_id.create();
	 					pop_up_same_id.add_description('Ilyen om azonosító már szerepel a táblában! Sor:'+res_txt['line']);
	 					pop_up_same_id.show();
	 				}
	 				else if(res_txt['Error'] == 'not seven')
	 				{
	 					pop_up_seven.create();
	 					pop_up_seven.add_description('Az okmányazonosító csak 7-essel kezdődhet! Sor:'+res_txt['line']);
	 					pop_up_seven.show();
	 				}
	 				else if(res_txt['Error'] == 'none')
	 				{
	 					pop_up_done.create();
	 					pop_up_done.show();
	 				}
	 			});		
	 		});
		}
		else
		{
			pop_up_ext.create();
			pop_up_ext.show();
		}
	}
	else
	{
		pop_up_not_chosen.create();
		pop_up_not_chosen.show();
	}
}