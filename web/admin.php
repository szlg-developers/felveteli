<?php 
	session_start();
	if(!isset($_SESSION['admin']))
	{
		exit();
	}
	include 'backend/includes/csrf_generate.php';
	$hash = csrf_generate('admin_upload');

?>
<!DOCTYPE html>
<html>
<head>
	<title>Felv admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="assets/style/admin.css">
	<link rel="stylesheet" type="text/css" href="assets/style/pop_up.css">
	<script type="text/javascript" src="assets/script/sheet_functions.js"></script>
	<script type="text/javascript" src="assets/script/admin.js"></script>
	<script type="text/javascript"> const csrf = '<?php echo $hash; ?>';</script>
</head>
<body>
	<div id="main_div">	
		<p id="title">File feltöltése</p>
		<p id="desc">Feltöltés leírása</p>
		<div>
			<input type="file" name="f">
			<button id="upbtn" onclick="upload_txt(this);">Feltöltés</button>
		</div>
	</div>
</body>
</html>